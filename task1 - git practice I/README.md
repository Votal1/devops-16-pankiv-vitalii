GIT TASK 1

Merge creates one commit, while rebase re-writes history of commits (places commmits in top).
Pros of merge: safe, non-destructive.
Cons: not clean history
Pros of rebase: clean history, traceability.
Cons: re-writes project history
Do not use rebase in public branches! Rebase will be useful when you work in team and other developer made unrelated changes. Merge would be better if you work alone.

![1](https://gitlab.com/Votal1/devops-16-pankiv-vitalii/-/blob/main/task1%20-%20git%20practice%20I/1.png)

![2](https://gitlab.com/Votal1/devops-16-pankiv-vitalii/-/blob/main/task1%20-%20git%20practice%20I/2.png)

![3](https://gitlab.com/Votal1/devops-16-pankiv-vitalii/-/blob/main/task1%20-%20git%20practice%20I/3.png)

GIT COURSE

![4](https://gitlab.com/Votal1/devops-16-pankiv-vitalii/-/blob/main/task1%20-%20git%20practice%20I/course.png)

LEARN GIT BRANCHING

![5](https://gitlab.com/Votal1/devops-16-pankiv-vitalii/-/blob/main/task1%20-%20git%20practice%20I/lgb1.png)

![6](https://gitlab.com/Votal1/devops-16-pankiv-vitalii/-/blob/main/task1%20-%20git%20practice%20I/lgb2.png)

![7](https://gitlab.com/Votal1/devops-16-pankiv-vitalii/-/blob/main/task1%20-%20git%20practice%20I/lgb3.png)
